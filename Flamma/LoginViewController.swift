//
//  ViewController.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//

import UIKit
import Combine

class LoginViewController: UIViewController {

	@IBOutlet weak var loginField: UITextField!
	@IBOutlet weak var passwordField: UITextField!
	private var anyCancellables: Set<AnyCancellable> = Set<AnyCancellable>()

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additio   nal setup after loading the view.
	}

	@IBAction func loginClicked(_ sender: Any) {
		let authManager = AuthManager(oauth: OAuth2EmailPasswordGrant(
			settings: [
				"client_id": "flamma_api",
				"client_secret": "fla_h2yhrh2837ryh*$8!",
				"token_uri": "https://flamma-app.nl/api/auth",
				"authorize_url": "https://flamma-app.nl/api/auth",
				"secret_in_body": true,
				"keychain": true
		]))

		print(authManager.login(email: "iostest", password: "123456").sink(receiveCompletion: { (complection) in
			switch complection {
			case .failure(let error):
				print(error)
			case .finished:
				let vc =  UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapViewController") as! MapViewController
				vc.modalPresentationStyle = .fullScreen
				self.show(vc, sender: nil)
			}
		}, receiveValue: { })
			.store(in: &anyCancellables))
	}

}

