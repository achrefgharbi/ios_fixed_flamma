//
//  NetworkObserver.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//

import Foundation
import Network

//sourcery: AutoMockable
protocol NetworkObserverProtocol {

	var isConnectedToInternet: Bool { get }

	func startMonitoringNetwork()
}

final class NetworkObserver: NetworkObserverProtocol {

	private(set) var isConnectedToInternet: Bool = true
	private var monitor: NWPathMonitorProtocol

	static let shared = NetworkObserver(monitor: NWPathMonitor())

	init(monitor: NWPathMonitorProtocol) {
		self.monitor = monitor
	}

	func startMonitoringNetwork() {
		monitor.start(queue: .init(label: "locum_network_monitor"))
		monitor.pathUpdateHandler = { [weak self] path in
			self?.isConnectedToInternet = path.status == .satisfied
		}
	}
}

protocol NWPathMonitorProtocol {

	var pathUpdateHandler: ((NWPath) -> Void)? { get set }

	func start(queue: DispatchQueue)
}

extension NWPathMonitor: NWPathMonitorProtocol {}
