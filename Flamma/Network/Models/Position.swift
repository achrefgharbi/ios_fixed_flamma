//
//  Position.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//

import Foundation

struct Position: Codable {
	let x: Double
	let y: Double
}
