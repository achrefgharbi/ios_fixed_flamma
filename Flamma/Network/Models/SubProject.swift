//
//  SubProject.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//

import Foundation

struct SubProject: Codable {
	let id: Int
	let current: Int
	let blueprint_image_url: String
	let name: String
	let installations: [Installation]
}
