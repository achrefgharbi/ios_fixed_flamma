//
//  OAuth2GrantProtocol.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//


import p2_OAuth2

//sourcery: AutoMockable
protocol OAuth2GrantProtocol {

	var refreshToken: String? { get }
	var isAuthorizing: Bool { get }
	var logger: OAuth2Logger? { get set }

	func hasUnexpiredAccessToken() -> Bool
	func authorize(params: OAuth2StringDict?, callback: @escaping ((OAuth2JSON?, OAuth2Error?) -> Void))
	func sign(_ url: URLRequest) throws -> URLRequest
	func forgetTokens()
	func forgetClient()
	func updateCredentials(email: String, password: String)
}

extension OAuth2EmailPasswordGrant: OAuth2GrantProtocol {

	func sign(_ url: URLRequest) throws -> URLRequest {
		let access = clientConfig.accessToken ?? ""
		var signedUrl = url
		signedUrl.setValue("Bearer \(access)", forHTTPHeaderField: "Authorization")
		return signedUrl
	}
}
