//
//  FlammaService.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//

import Combine
import Moya

protocol FlammaServiceProtocol {
	func subProject(id: Int) -> AnyPublisher<SubProject, Error>
}

class FlammaService: FlammaServiceProtocol {

	private let client: MoyaClient<FlammaTarget>

	init(client: MoyaClient<FlammaTarget>) {
		self.client = client
	}

	func subProject(id: Int) -> AnyPublisher<SubProject, Error> {
		return client
			.request(.subProject(id: id))
		.eraseToAnyPublisher()
	}
}
