//
//  Target.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//


import Moya

enum FlammaTarget: TargetType {

	case subProject(id: Int)
	

	var baseURL: URL { URL(string: "https://flamma-app.nl/api/")! }

	var path: String {
		switch self {
		case .subProject(let id):
			return "project/get-subproject/\(id)"
		}
	}
	var method: Method {
		switch self {
		case .subProject:
			return .get

		}
	}

	var sampleData: Data { Data() }

	var task: Task {
		switch self {
		case .subProject:
			return .requestPlain
		}
	}

	var headers: [String: String]? { nil }

}
