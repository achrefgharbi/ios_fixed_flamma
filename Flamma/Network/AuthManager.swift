//
//  AuthManager.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//


import Foundation
import p2_OAuth2
import Moya
import Combine

protocol HasAuthManager: class {
	//swiftlint:disable:next implicitly_unwrapped_optional
	var authManager: AuthManager! { get set }

}

//sourcery: AutoMockable
protocol AuthManagerProtocol: class {

	/// A boolean indicating if a user is logged in or not.
	var isUserLoggedIn: Bool { get }

	/// Try to login with an email and a password.
	/// - Parameters:
	///   - email: the email of the user.
	///   - password: the password that goes with the email.
	/// - Returns: Future<Void, Error>
	func login(email: String, password: String) -> AnyPublisher<Void, Error>

	/// Sign a request with the credentials of the current user.
	///
	/// - Parameter request: The request to sign.
	/// - Returns: AnyPublisher<URLRequest, Error>
	func sign(_ request: URLRequest) -> AnyPublisher<URLRequest, Error>

	/// Logout the curret user.
	func logout()
}

class AuthManager: AuthManagerProtocol {

	// MARK: - Properties
	private var oauth: OAuth2GrantProtocol
	var isUserLoggedIn: Bool {
		return oauth.hasUnexpiredAccessToken() || oauth.refreshToken != nil
	}

	init(oauth: OAuth2GrantProtocol) {
		self.oauth = oauth
		self.oauth.logger = OAuth2DebugLogger(.trace)
	}

	func login(email: String, password: String) -> AnyPublisher<Void, Error> {
		let params = ["username": email, "password": password]
		oauth.forgetTokens()
		oauth.updateCredentials(email: email, password: password)
		return authorize(params: params, fromLogin: true).eraseToAnyPublisher()
	}

	func logout() {
		oauth.forgetTokens()
	}

	func sign(_ request: URLRequest) -> AnyPublisher<URLRequest, Error> {
		guard isUserLoggedIn, !oauth.hasUnexpiredAccessToken(), !oauth.isAuthorizing else {
			do {
				return try Just(oauth.sign(request)).setFailureType(to: Error.self).eraseToAnyPublisher()
			} catch {
				return Fail(error: error).eraseToAnyPublisher()
			}
		}
		return authorize()
			.tryMap { [oauth] _ in try oauth.sign(request) }
			.eraseToAnyPublisher()
	}
}

// MARK: - Privates
private extension AuthManager {

	private func authorize(params: OAuth2StringDict? = nil, fromLogin: Bool = false) -> Future<Void, Error> {
		return Future { [oauth] promise in
			oauth.authorize(params: params) {[weak self] (_, error) in
				if let error = error {
					promise(.failure(error))
					// Log out
					oauth.forgetTokens()
					// Navigate to login if not already
					guard !fromLogin else { return }
				} else {
					promise(.success(()))
				}
			}
		}
	}
}
