//
//  MapViewController.swift
//  Flamma
//
//  Created by Achref Gharbi on 02/06/2020.
//  Copyright © 2020 DTT. All rights reserved.
//

import UIKit
import Combine
import Nuke
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {

	private var anyCancellables: Set<AnyCancellable> = Set<AnyCancellable>()
	var subProject: SubProject?
	var image: UIImage?

	@IBOutlet weak var mapView: MKMapView!
	let maxLat: Float = 25.0
	let maxLong: Float = 60.0


	override func viewDidLoad() {
		super.viewDidLoad()

		mapView.delegate = self

		let authManager = AuthManager(oauth: OAuth2EmailPasswordGrant(
			settings: [
				"client_id": "flamma_api",
				"client_secret": "fla_h2yhrh2837ryh*$8!",
				"token_uri": "https://flamma-app.nl/api/auth",
				"authorize_url": "https://flamma-app.nl/api/auth",
				"secret_in_body": true,
				"keychain": true
		]))
		let flammaService = FlammaService(client: MoyaClient<FlammaTarget>(
			authManager: authManager,
			networkObserver: NetworkObserver.shared
		))
		flammaService.subProject(id: 847).sink(receiveCompletion: { (completion) in
			switch completion {
			case .failure(let error):
				print(error)
			case .finished:
				print(self.subProject)
				self.downloadImage(url: self.subProject?.blueprint_image_url ?? "")
			}
		}) { sub in
			self.subProject = sub
		}
	.store(in: &anyCancellables)
	}

	func downloadImage(url: String) {
		let task = ImagePipeline.shared.loadImage(with: URL(string: "https://flamma-app.nl/" + url)!, queue: nil, progress: nil) { result in
			switch result {
			case .failure(let error):
				print(error)
			case .success(let response):
				self.image = response.image
				self.calculateRatio()
			}
		}
	}

	func calculateRatio() {
		guard let image = image else {
			return
		}
		let width = image.cgImage!.width
		let height = image.cgImage!.height
		var latitude = maxLat
		var longitude: Float = 0.0

		if width > height {
			let factor = Float(height) / Float(width);
			longitude = maxLong * factor;
		} else {
			let factor =  Float(width) / Float(height);
			longitude = maxLat * factor
		}

	//let southEast = CLLocationCoordinate2D(latitude: .init(0 - (latitude / 2.0)), longitude: .init(0 - (longitude / 2.0)))
		let southWest = CLLocationCoordinate2D(latitude: .init(latitude / 2.0), longitude: .init(0 - (longitude / 2.0)))
		let northEast = CLLocationCoordinate2D(latitude: .init(0 - (latitude / 2.0)), longitude: .init(longitude / 2.0))
		//let northWest = CLLocationCoordinate2D(latitude: .init(latitude / 2.0), longitude: .init(longitude / 2.0))
		let southWestMapPoint = MKMapPoint(southWest)
		let northEastMapPoint = MKMapPoint(northEast)
		let northwestMapPoint = MKMapPoint(x: southWestMapPoint.x, y: northEastMapPoint.y);
		let mapRectWidth: Double = northEastMapPoint.x - northwestMapPoint.x
		let mapRectHeight: Double = northwestMapPoint.y - southWestMapPoint.y;

		let mapRect = MKMapRect(x: southWestMapPoint.x, y: southWestMapPoint.y, width: mapRectWidth, height: mapRectHeight)
	//	let region = MKCoordinateRegion(mapRect)
	//	mapView.setRegion(region, animated: true)
		mapView.setCameraBoundary(MKMapView.CameraBoundary.init(mapRect: mapRect), animated: false)
		let overlay = ImageOverlay(image: image, rect: mapRect)
		mapView.addOverlay(overlay)
		mapView.addAnnotations(	(subProject?.installations.map { installation in
			let annotation = MKPointAnnotation()
			annotation.coordinate = calculateLatLong(x: installation.position.x, y: installation.position.y, northEast: northEast, southwest: southWest)
			annotation.title = installation.name
			return annotation
			}) ?? [])
//		subProject?.installations.map { installation in
//			let annotation = MKPointAnnotation()
//			annotation.coordinate = calculateLatLong(x: installation.position.x, y: installation.position.y, northEast: northEast, southwest: southWest)
//			return annotation
//		}
	}

	func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {

		if overlay is ImageOverlay {
			return ImageOverlayRenderer(overlay: overlay)
		}
			return MKOverlayRenderer(overlay: overlay)
	}

	func calculateLatLong(x: Double, y: Double, northEast: CLLocationCoordinate2D, southwest: CLLocationCoordinate2D) -> CLLocationCoordinate2D {

		let maxX = (northEast.longitude - southwest.longitude)
		let maxY = (northEast.latitude - southwest.latitude)
		let longitude = (southwest.longitude + (maxX * x))
		let latitude = -(northEast.latitude - (maxY * y))

			return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
	}
}

class ImageOverlay : NSObject, MKOverlay {

	let image:UIImage
	let boundingMapRect: MKMapRect
	let coordinate:CLLocationCoordinate2D

	init(image: UIImage, rect: MKMapRect) {
		self.image = image
		self.boundingMapRect = rect
		self.coordinate = rect.origin.coordinate
	}
}

class ImageOverlayRenderer : MKOverlayRenderer {

	override func draw(_ mapRect: MKMapRect, zoomScale: MKZoomScale, in context: CGContext) {

		guard let overlay = self.overlay as? ImageOverlay else {
			return
		}

		let rect = self.rect(for: overlay.boundingMapRect)

		UIGraphicsPushContext(context)
		overlay.image.draw(in: rect)
		UIGraphicsPopContext()
	}
}
